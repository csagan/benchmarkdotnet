﻿using System;
using BenchmarkDotNet.Attributes;

namespace BenchmarkTest
{
    public class ConvertBenchmark
    {
        const int Max = 500;

        enum OrderType
        {
            Type1,
            Type2,
            Type3
        }

        [Benchmark]
        public void SingleConvert()
        {
            var value = Convert.ToInt32(OrderType.Type2);

            for (int i = 0; i < Max; i++)
            {
                var t = i;
            }
        }

        [Benchmark]
        public void MultipleConvert()
        {
            for (int i = 0; i < Max; i++)
            {
                var value = Convert.ToInt32(OrderType.Type2);
            }
        }
    }
}
