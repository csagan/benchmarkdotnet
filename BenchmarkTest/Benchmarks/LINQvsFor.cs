﻿using System.Collections.Generic;
using System.Linq;
using BenchmarkDotNet.Attributes;

namespace BenchmarkTest
{
    public class LINQvsFor
    {
        private List<string> list = Enumerable.Range(1, 2000).Select(t => t + "name").ToList();

        [Benchmark]
        public List<string> For()
        {
            List<string> retVal = new List<string>();
            for (int i = 0; i < list.Count; i++)
            {
                if (list[i].Contains("1"))
                {
                    retVal.Add(list[i]);
                }
            }
            return retVal;
        }

        [Benchmark]
        public List<string> LINQ()
        {
            return list.Where(t => t.Contains("1")).ToList();
        }
    }
}
