﻿using BenchmarkDotNet.Attributes;

namespace BenchmarkTest
{
    public class StringBenchmark
    {
        const int Max = 1000000;
        readonly int _cats = int.Parse("10");
        readonly int _dogs = int.Parse("2");

        [Benchmark]
        public void Interpolation()
        {
            for (int i = 0; i < Max; i++)
            {
                string result = $"{_cats} cats and {_dogs} dogs";
                if (result[0] != '1')
                {
                    return;
                }
            }
        }

        [Benchmark]
        public void Format()
        {
            for (int i = 0; i < Max; i++)
            {
                string result = string.Format("{0} cats and {1} dogs", _cats, _dogs);
                if (result[0] != '1')
                {
                    return;
                }
            }
        }

        [Benchmark]
        public void Concat()
        {
            for (int i = 0; i < Max; i++)
            {
                string result = _cats + " cats and " + _dogs + " dogs";
                if (result[0] != '1')
                {
                    return;
                }
            }
        }
    }
}
